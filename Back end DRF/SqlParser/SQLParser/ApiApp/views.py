from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
#import sqlite3
import sqlparse
from mo_sql_parsing import parse
            
# Create your views here.
class BookApiView(APIView):
    
    def post(self,request):
        query_set = request.data["sqlquery"].split(';')
        print(query_set)
        final_query = ""
        count = 1
        query_set_new = []
        for query in query_set:
            query = query.strip()
            if not query:
                continue
            query = sqlparse.format(query, reindent=True, keyword_case='upper')
            query_set_new.append(query)
        final_query = '; \n'.join(query_set_new)
        for query in query_set_new:
            
            print("Query is : ",query)
            #query = sqlparse.format(query, reindent=True, keyword_case='upper')
            
            message = ""
            success = True
            try:
                paring = parse(query)
            except Exception as e:
                er = str(e)
                print("*"*12)
                print(query)
                print(er)
                print("*"*12)
                success = False
                message = "Error in Query number "+str(count) +"!  \n" +er
                break
            count = count + 1
            '''
            conn = sqlite3.connect('test_database') 
            c = conn.cursor()
            try:
                c.execute(query)
            except Exception as e:
                er = str(e)
                print(er)
                if er.startswith("no such table:"):
                    success = True
                    message = ""
                else:
                    success = False
                    message = er
                    break
            '''    
        print(final_query)
        return Response({'success':success,'message':message,'query': final_query})